import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardsComponent } from './cards/cards.component';
import { CardComponent } from './card/card.component';
import { RoomsComponent } from './rooms/rooms.component';
import { HttpClientModule } from '@angular/common/http';
import { RoomsService } from './rooms.service';


@NgModule({
  declarations: [
    AppComponent,
    CardsComponent,
    CardComponent,
    RoomsComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    RoomsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
