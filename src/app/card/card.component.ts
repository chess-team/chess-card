import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
// tslint:disable-next-line: ban-types
@Input() number: Number;
selected = 'default';
  clickCard() {
    if ( this.selected === 'default' ) {
      this.selected = 'hidden';
    } else if ( this.selected === 'hidden') {
      this.selected = 'visible';
    } else {
      this.selected = 'default';
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
