import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class RoomsService {
  constructor(private http: HttpClient) { }
  // tslint:disable-next-line: contextual-lifecycle
    getRooms(): Observable<any> {
    // tslint:disable-next-line: prefer-const
    let rooms = this.http.get('http://localhost:8888/drupal/jsonapi/node/stanza');
    return rooms;
  }
}
