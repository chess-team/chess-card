import { RoomsService } from '../rooms.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {
  rooms: any;
  clicked = 'button_roomDefault';
  // tslint:disable-next-line: no-shadowed-variable
  constructor(private roomsService: RoomsService) {}
  openRoom() {
    if ( this.clicked === 'button_roomDefault' ) {
      this.clicked = 'button_roomHidden';
    } else {
      this.clicked = 'button_roomDefault';
    }
  }
  enterRoom() {
    console.log('sei entrato nella stanza');
  }
  getRooms(): void {
    this.roomsService.getRooms()
        .subscribe(rooms => this.rooms = rooms);
  }
  ngOnInit() {
    this.rooms = this.getRooms();
  }
}
